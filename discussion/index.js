console.log("Hello World!");


//IF
let numA = -1;

if(numA<0){
	console.log("Hello!");
}

console.log(numA<0);

if(numA>0){
	console.log("This statement will not be printed!");
}

//ELSE IF
let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York!");
}

let numB = 1;
if (numA > 0) {
	console.log("Hello!");
}
else if (numB > 0){
	console.log("World!");
}

city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York!");	
}
else if (city === "Tokyo"){
	console.log("Welcome to Tokyo!");
}


//ELSE

if (numA > 0){
	console.log("Hello");
}
else if (numB === 0){
	console.log("World");
}
else{
	console.log("Again");
}

let age = prompt("How old are you?");

if (age <= 18){
	console.log("Not Allowed to Drink");
}
else{
	console.log("Matanda ka na, shot na!");
}

/*
	MINIACTIVITY 
	
	Create a condi for height < 150 cm
		display did not pass min height req
	Create a condi for height > 150 cm
		display passed the min height req
	use function

*/

function heightCheck(h) {
	console.log("Your height is "+h);
	if (h<150){
		console.log("Did not pass the minimum height requirement.");
	}
	else if (h>=150){
		console.log("Passed the minimum height requirement.");
	}
	else if (h<=0){
		console.log("Please enter a valid height.");
	}
}

heightCheck(prompt("Enter your height:"));

let message = "No message.";
console.log(message);

function detTyphoonInt(ws){
	if (ws < 30){
		return "Not a typhoon yet."
	}
	else if (ws <= 61){
		return "Tropical depression detected."
	}
	else if (ws >= 62 && ws <= 88){
		return "Tropical storm detected."
	}
	else if (ws >= 89 && ws <= 117){
		return "Severe tropical storm detected."
	}
	else {
		return "Typhoon detected"
	}
}
message = detTyphoonInt(prompt("Enter windspeed (in mph): "));
console.log(message);

if (message == "Typhoon detected"){
	console.warn("Be careful!");
}

/*TRUTHY AND FALSY*/
/*
	1. false
	2. 0
	3. -0
	4.""
	5.null
	6.undefined
	7.NaN
*/
// TRUTHY EX.

if (true) {
	console.log("truthy!");
}
if (1) {
	console.log("truthy!");
}
if ([]) {
	console.log("truthy!");
}

// FALSY EX.
if (false) {
	console.log("falsy!");
}
if (0) {
	console.log("falsy!");
}
if (undefined) {
	console.log("falsy!");
}
// these values are considered false!

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day)

	switch (day) {
		case 'monday':
			console.log("The color of the day is red");
			break;
		case 'tuesday':
			console.log("The color of the day is orange");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow");
			break;
		case 'thursday':
			console.log("The color of the day is green");
			break;
		case 'friday':
			console.log("The color of the day is blue");
			break;
		case 'saturday':
			console.log("The color of the day is indigo");
			break;
		case 'sunday':
			console.log("The color of the day is violet");
			break;
		// case ('saturday' || 'sunday'):
		// 	console.log("The color of the day is indigo");
		// 	break;
		default:
			console.log("Please input a valid day!");
			break;
	}

/*//Try-Catch-Finally Statement
function showIntensityAlert(windSpeed) {
	try {
		//Attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed))
	}

	catch (error) {

		//catch errors within "try" statement
		console.log(error)
		console.warn(error.message)
	}

	finally {
		//Continue execution of code regardless of success or failure of code execution in the "try" block to handle/resolve errors
		alert("Intesity updates will show new alert.")
	}

showIntensityAlert(56)*/