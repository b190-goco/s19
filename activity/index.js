console.log("Hello World");

let username, password, role;
/*
function userLogin() {
	let r1 = 0, r2 = 0, r3 = 0;
	while (r1 == 0) {
		username = prompt("Enter your username:");
		r1 = checkNullInput();
	}
	while (r2 == 0) {
		password = prompt("Enter your password:");
		r2 = checkNullInput();
	}
	while (r3 == 0){
		role = prompt("Enter your role:");
		r3 = checkNullInput();
	}
	function checkNullInput() {
		if (username === "" || password === "" || role === "") {
			alert("Input cannot be empty.");
			return 0;
		}
		else {
			return 1;
		}
	}
}
*/
function userLogin() {
	username = prompt("Enter your username:");
	password = prompt("Enter your password:");
	role = prompt("Enter your role:").toLowerCase();
	if (username == "" || password == "" || role == "") {
		alert("Input cannot be empty.")
	}
	if (role === "admin") {
		alert("Welcome back to the class portal, admin!");
	}
	else if (role === "teacher") {
		alert("Thank you for logging in, teacher!");
	}
	else if (role === "rookie") {
		alert("Welcome to the class portal, student!");
	}
	else {
		alert("Role out of range.");
	}
}

userLogin();

function gradeCalculator(grade1, grade2, grade3, grade4) {
	// grade1 = Number(prompt("Enter your first score:"));
	// grade2 = Number(prompt("Enter your second score:"));
	// grade3 = Number(prompt("Enter your third score:"));
	// grade4 = Number(prompt("Enter your fourth score:"));
	let gradeAve = (grade1+grade2+grade3+grade4)/4;
	console.log("Raw Average: "+gradeAve);
	gradeAve = Math.round(gradeAve);
	console.log("Final Average: "+gradeAve);
	/*// fixed grading window to accommodate decimals
	// now, it accepts in between 74 and 75 as F.
	if (gradeAve < 75) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is F.");
	}
	else if (gradeAve >= 75 && gradeAve < 80) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is D.");
	}
	else if (gradeAve >= 80 && gradeAve < 85) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is C.");
	}
	else if (gradeAve >= 85 && gradeAve < 90) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is B.");
	}
	else if (gradeAve >= 90 && gradeAve < 96) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is A.");
	}
	else if (gradeAve >= 96) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is A+.");
	}*/
	if (gradeAve <= 74) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is F.");
	}
	else if (gradeAve >= 75 && gradeAve <= 80) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is D.");
	}
	else if (gradeAve >= 80 && gradeAve <= 85) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is C.");
	}
	else if (gradeAve >= 85 && gradeAve <= 90) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is B.");
	}
	else if (gradeAve >= 90 && gradeAve <= 95) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is A.");
	}
	// > 96 wont accept 96 input, used >=;
	else if (gradeAve >= 96) {
		console.log("Hello, student, your average is "+gradeAve+". The letter equivalent is A+.");
	}
}

// gradeCalculator();